/**
 * Created by Seksit.Y on 06/18/2017.
 */
"use strict";

module.exports = (app) => {
  let router;
  app.use("/api/v1", router = require("./v1"));
  // app.use("/api/v2", router = require("./v2"));
  return router;
}
