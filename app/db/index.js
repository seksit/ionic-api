/**
 * Created by Seksit.Y on 06/24/2017.
 */
"use strict";
const config = require("../config");
const aws = require("aws-sdk");
aws.config.update(config.aws);
let docClient = new aws.DynamoDB.DocumentClient();

let db = {
  // Query : Directly access items from a table
  // by primary key or a secondary index
  query: (params, callback) => {
    let items = [];
    docClient.query(params, function onQuery(err, data) {
      if (err) {
        return callback({
          "success": false,
          "data": err
        });
      }
      items.push.apply(items, data.Items);
      if (params.Limit) {
        return callback({
          "success": true,
          "count": items.length,
          "data": items
        });
      }
      // continue scanning if we have more movies, because
      // scan can retrieve a maximum of 1MB of data
      // console.log(params.Limit ? "Limit" : "No Limit");
      if (typeof data.LastEvaluatedKey !== "undefined") {
        params.ExclusiveStartKey = data.LastEvaluatedKey;
        docClient.query(params, onQuery);
      } else {
        return callback({
          "success": true,
          "count": items.length,
          "data": items
        });
      }
    });
  },
  // Scan : One or more items and item attributes by accessing
  // every item in a table or a secondary index
  scan: (params, callback) => {
    let items = [];
    docClient.scan(params, function onScan(err, data) {
      if (err) {
        return callback({
          "success": false,
          "data": err
        });
      }
      items.push.apply(items, data.Items);
      // continue scanning if we have more movies, because
      // scan can retrieve a maximum of 1MB of data
      if (typeof data.LastEvaluatedKey !== "undefined") {
        params.ExclusiveStartKey = data.LastEvaluatedKey;
        docClient.scan(params, onScan);
      } else {
        return callback({
          "success": true,
          "count": items.length,
          "data": items
        });
      }
    });
  },
  // putItem : Put one item on the table
  put: (params, callback) => {
    docClient.put(params, function onPut(err, data) {
      if (err) {
        return callback({
          "success": false,
          "data": err
        });
      }
      return callback({
        "success": true,
        "data": data
      });
    });
  }
}

module.exports = db;
