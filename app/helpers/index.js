/**
 * Created by Seksit.Y on 06/24/2017.
 */
"use strict";
const db = require("../db");
const TABLE_NAME = "ionics";

let ionic = {
  // Find Last item
  findLast: (user_id, callback) => {
    ionic.findLastIndex(user_id, 1, callback);
  },
  // Find last N items
  findLastIndex: (user_id, n, callback) => {
    let params = {
      TableName: TABLE_NAME,
      KeyConditionExpression: "#user_id = :user_id",
      ExpressionAttributeNames: {
        "#user_id": "user_id"
      },
      ExpressionAttributeValues: {
        ":user_id": user_id
      },
      ScanIndexForward: false,
      Limit: n
    }
    db.query(params, callback);
  },
  // Find items in last N hours
  findLastHours: (user_id, n, callback) => {
    ionic.findLast(user_id, (data) => {
      let t_stamp_at_n_hours = new Date(0);
      // Get timestamp of item before N hours
      t_stamp_at_n_hours.setUTCSeconds(data.data[0].payload.t_stamp);
      t_stamp_at_n_hours.setHours(t_stamp_at_n_hours.getHours() - n);
      t_stamp_at_n_hours = t_stamp_at_n_hours.getTime()/1000;
      // Scan database
      let params = {
        TableName: TABLE_NAME,
        KeyConditionExpression: "#user_id = :user_id and #t_stamp >= :t_stamp_at_n_hours",
        ExpressionAttributeNames: {
          "#user_id": "user_id",
          "#t_stamp": "t_stamp"
        },
        ExpressionAttributeValues: {
          ":user_id": user_id,
          ":t_stamp_at_n_hours": t_stamp_at_n_hours.toString()
        },
        ScanIndexForward: false
      }
      db.query(params, callback);
    });
  },
  // Find items by User ID
  findById: (user_id, callback) => {
    let params = {
      TableName: TABLE_NAME,
      KeyConditionExpression: "#user_id = :user_id",
      ExpressionAttributeNames: {
        "#user_id": "user_id"
      },
      ExpressionAttributeValues: {
        ":user_id": user_id
      }
    }
    db.query(params, callback);
  },
  // Find items by timestamp (t_stamp)
  findByTimestamp: (user_id, t_stamp, callback) => {
    let params = {
      TableName: TABLE_NAME,
      KeyConditionExpression: "#user_id = :user_id AND #t_stamp = :t_stamp",
      ExpressionAttributeNames: {
        "#user_id": "user_id",
        "#t_stamp": "t_stamp"
      },
      ExpressionAttributeValues: {
        ":user_id": user_id,
        ":t_stamp": t_stamp
      }
    }
    db.query(params, callback);
  },
  // Create new item by User ID
  putItem: (user_id, item, callback) => {
    let params = {
      TableName: TABLE_NAME,
      Item: item
    }
    db.put(params, callback);
  }
}

module.exports = {
  ionic
}
