/**
 * Created by Seksit.Y on 06/18/2017.
 */
"use strict";
const fitbit = require("./app");
const body_parser = require("body-parser");
const express = require("express");
const app = express();
// define server listen port
app.set("port", process.env.PORT || 3000);
// parse application/json
app.use(body_parser.json());
// parse application/x-www-form-urlencoded
app.use(body_parser.urlencoded({ extended: true }));
// allow access control
app.use((req, res, next) => {
	// allow connect to website
	res.header("Access-Control-Allow-Origin" , "*");
	// allow request methods
	res.header("Access-Control-Allow-Methods", "GET, POST, OPTIONS, PUT, PATCH, DELETE");
	// allow request headers
	res.header("Access-Control-Allow-Headers", "X-Requested-With, Content-Type, Cache-Control");
	// allow request with cookies(true) to API (e.g. in case use sessions)
	res.header("Access-Control-Allow-Credentials", true);
	// pass to next middleware
	return next();
});
// Router middleware
app.use("/", fitbit.router(app));
// Start server
app.listen(app.get("port"), () => {
	console.log("Fitbit API server running on port: " + app.get("port"));
});
