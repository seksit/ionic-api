/**
 * Created by Seksit.Y on 06/18/2017.
 */
"use strict";
const helpers = require("./helpers");

module.exports = {
  router: (app) => require("./routes")(app)
}
