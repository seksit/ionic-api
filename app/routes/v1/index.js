/**
 * Created by Seksit.Y on 06/18/2017.
 */
"use strict";
const v1 = require("express").Router();
const helpers = require("../../helpers");
const url = require("url");

v1.route("/:id/last")
  .get((req, res, next) => {
    // console.log(url.parse(req.url, true));
    helpers.ionic.findLast(req.params.id, (data) => {
      res.json(data);
    });
  });

v1.route("/:id/last-index")
  .get((req, res, next) => {
    // console.log(url.parse(req.url, true));
    helpers.ionic.findLastIndex(req.params.id, req.query.index, (data) => {
      res.json(data);
    });
  });

v1.route("/:id/last-hours")
  .get((req, res, next) => {
    // console.log(url.parse(req.url, true));
    helpers.ionic.findLastHours(req.params.id, req.query.hours, (data) => {
      res.json(data);
    });
  });

v1.route("/:id")
  .get((req, res, next) => {
    // console.log(url.parse(req.url, true));
    helpers.ionic.findById(req.params.id, (data) => {
      res.json(data);
    });
  })
  .post((req, res, next) => {
    // console.log(url.parse(req.url, true));
    let item = JSON.parse(req.body.item);
    helpers.ionic.putItem(req.params.id, item, (data) => {
      res.json(data);
    })
  })

v1.route("/:id/t_stamp")
  .get((req, res, next) => {
    // console.log(url.parse(req.url, true));
    helpers.ionic.findByTimestamp(req.params.id, req.query.t_stamp, (data) => {
      res.json(data);
    });
  });

module.exports = v1;
