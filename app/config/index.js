/**
 * Created by Seksit.Y on 06/24/2017.
 */
"use strict";
if (process.env.NODE_ENV === "production") {
  // Production environment variable
  module.exports = {
    aws: {
      region: process.env.REGION,
      endpoint: process.env.ENDPOINT
    }
  }
} else {
  // Development environment variable
  module.exports = require("./development.json");
}
